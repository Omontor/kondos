<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>{{ config('app.name', 'Laravel') }} </title>
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/responsive.css">
</head>

<body>
    <div class="preloader"></div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one ">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="/">
                            <img src="images/logo-1-1.png" class="main-logo" width="97" alt="Awesome Image" />
                        </a>
                        <button class="menu-toggler" data-target=".main-navigation">
                            <span class="fa fa-bars"></span>
                        </button>
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                        <ul class=" navigation-box one-page-scroll-menu ">
                         <li class="scrollToLink">
                                <a href="#home">Inicio</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#services">Funciones</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#screens">Capturas</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#pricing">Costo</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#blog">Blog</a>
                                <ul class="sub-menu">
                                   <!-- <li><a href="blog.html">Ver todos los post</a></li> -->
                                </ul><!-- /.sub-menu -->
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class="right-side-box">
                        <a href="/login" class="thm-btn header-one__btn">Iniciar Sesión</a>
                    </div><!-- /.right-side-box -->
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.header-one -->
        <section class="banner-one" id="home">
            <div class="container">
                <div class="banner-one__moc-wrap">
                    <img src="images/tel1.png" class="banner-one__moc" alt="Awesome Image" />
                </div><!-- /.banner-one__moc-wrap -->
                <div class="row justify-content-end">
                    <div class="col-lg-5 col-md-6">
                        <div class="banner-one__content">
                            <h3 class="banner-one__title">Administra<br> tu condominio <br> eficientemente</h3>
                            <!-- /.banner-one__title -->
                            <p class="banner-one__text">De manera sencilla, automatizada y segura.</p><!-- /.banner-one__text -->
                            <a href="#" class="banner-one__btn thm-btn">Descarga la aplicación</a>
                        </div><!-- /.banner-one__content -->
                    </div><!-- /.col-lg-5 -->
                </div><!-- /.row justify-content-end -->
            </div><!-- /.container -->
        </section><!-- /.banner-one -->

    <!--    Inicia el slider // Se tienen que meter los logos de clientes recientes
        <section class="brand-one">
            <div class="container">
                <div class="brand-one__carousel owl-theme owl-carousel">
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                    <div class="item">
                        <img src="images/brand-1-1.png" alt="Awesome Image" />
                    </div>
                </div>
            </div>
        </section> -->
        <section class="service-one" id="services">
            <div class="container">
                <div class="block-title text-center">
                    <h2 class="block-title__title"> ¿QUÉ TE OFRECE  <br> KONDOS?</h2><!-- /.block-title__title -->
                </div><!-- /.block-title -->
                <div class="row">
                    <div class="col-lg-4">
                        <div class="service-one__single">
                            <div class="service-one__icon">
                                <i class="appyn-icon-target-1"></i>
                            </div><!-- /.service-one__icon -->
                            <h3 class="service-one__title"><a href="#">Navegación sencilla<br>e intuitiva</a></h3>
                            <!-- /.service-one__title -->
                            <p class="service-one__text">Menus legibles y de fácil acceso.<br> No necesitas conocimientos especiales para poder utiliazarla.</p><!-- /.service-one__text -->
                        </div><!-- /.service-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="service-one__single">
                            <div class="service-one__icon">
                                <i class="appyn-icon-businessman"></i>
                            </div><!-- /.service-one__icon -->
                            <h3 class="service-one__title"><a href="#">CONTROL DE USUARIOS<br> CON PRIVILEGIOS CONTROLADOS.</a></h3>
                            <!-- /.service-one__title -->
                            <p class="service-one__text">Cada usuario tiene roles y funciones habilitadas especialmente para él.<br>
Condómino, Administrador, Seguridad, Director de junta administrativa, tesoreros, etc.</p><!-- /.service-one__text -->
                        </div><!-- /.service-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="service-one__single">
                            <div class="service-one__icon">
                                <i class="appyn-icon-paper-plane"></i>
                            </div><!-- /.service-one__icon -->
                            <h3 class="service-one__title"><a href="#">SEGURIDAD<br> TOTAL</h3>
                            <!-- /.service-one__title -->
                            <p class="service-one__text">Cada usuario cuenta con accesos personalizados irrepetibles, la información de los usuarios está encriptada <br>Y ES TRATADA DE MANERA CONFIDENCIAL.
Funciones de alerta y de seguridad siempre disponibles.</p><!-- /.service-one__text -->
                        </div><!-- /.service-one__single -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.service-one -->
        <div class="video-one">
            <div class="container">
                <div class="block-title text-center">
                    <h2 class="block-title__title">¿Cómo funciona <br> Kondos?</h2><!-- /.block-title__title -->
                </div><!-- /.block-title -->
                <div class="video-one__content">
                    <img src="images/video-1-1.jpg" alt="Awesome Image" />
                    <a href="https://www.youtube.com/watch?v=n-D1EB74Ckg" class="video-one__link video-popup"><i
                            class="fa fa-play"></i></a>
                </div><!-- /.video-one__content -->
            </div><!-- /.container -->
        </div><!-- /.video-one -->


        <section class="testimonials-one">
            <div class="container">
                <div class="row ">
                    <div class="col-lg-6">
                        <div class="testimonials-one__thumb-carousel">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="images/testimonials.png" alt="Awesome Image" />
                                </div><!-- /.swiper-slide -->
                                <div class="swiper-slide">
                                    <img src="images/testimonials.png" alt="Awesome Image" />
                                </div><!-- /.swiper-slide -->
                                <div class="swiper-slide">
                                    <img src="images/testimonials.png" alt="Awesome Image" />
                                </div><!-- /.swiper-slide -->
                            </div><!-- /.swiper-wrapper -->
                        </div><!-- /.testimonials-one__thumb-carousel -->
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6 d-flex">
                        <div class="my-auto">
                            <!-- <div class="block-title text-left">
                                <h2 class="block-title__title">What users are <br> talking about</h2>

                            </div>-->
                            <div class="testimonials-one__carousel">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="testimonials-one__single">
                                            <p class="testimonials-one__text">El sistema KONDOS está desarollado por ingenieros y diseñadores en conjunto
con administradores de condominios y fraccionamientos así como con residentes usuarios.
</p>
                                        </div><!-- /.testimonials-one__single -->
                                    </div><!-- /.swiper-slide -->
                                    <div class="swiper-slide">
                                        <div class="testimonials-one__single">
                                            <p class="testimonials-one__text">
Te ofrecemos un sistema confiable adecuado a las necesidades y funcionalidades
de la vida actual. Adaptable a cualquie tipo de Fraccionamientos, condominios y desarollos.</p>

                                        </div><!-- /.testimonials-one__single -->
                                    </div><!-- /.swiper-slide -->
                                    <div class="swiper-slide">
                                        <div class="testimonials-one__single">
                                            <p class="testimonials-one__text">
                                                <ul>
                                                    <li>
                                                        Lleva el control de tus apotaciones económicas.
                                                    </li>
                                                    <li>
                                                        Eficienta el tiempo de operación administrativa Agiliza y controla mejor los accesos
                                                    </li>
                                                     <li>
                                                        
Identifica areas de oportunidad Mejora la comunicación interna
                                                    </li>
                                                     <li>
                                                        Registra la actividad diaria Reportes confiables
                                                    </li>
                                                     <li>
                                                        Diseño amigable y navegación intuitiva Usuarios controlados
                                                    </li>
                                                    <li>Seguridad</li>
                                                </ul>






                                        </div><!-- /.testimonials-one__single -->
                                    </div><!-- /.swiper-slide -->
                                </div><!-- /.swiper-wrapper -->
                                <div class="swiper-pagination"></div>
                            </div><!-- /.testimonials-one__carousel -->
                        </div><!-- /.my-auto -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.testimonials-one -->
        


        <section class="blog-one" id="blog">
            <div class="container">
                <div class="block-title text-center">
                    <h2 class="block-title__title">Noticias y artículos<br> recientes</h2><!-- /.block-title__title -->
                </div><!-- /.block-title -->
                <div class="row">
                    <div class="col-lg-4">
                        <div class="blog-one__single">
                            <div class="blog-one__image">
                                <img src="{{$posts[2]->imageURL}}" alt="Post Image" />
                                <a a href="blog/{{$posts[2]->url}}"><i class="appyn-icon-plus-symbol"></i></a>
                            </div><!-- /.blog-one__image -->
                            <div class="blog-one__content">
                                <span class="blog-one__meta"><a href="#">{{$posts[2]->published_at->format('d m Y')}}</a></span>
                                <h3 class="blog-one__title"><a href="blog/{{$posts[2]->url}}">{{$posts[2]->title}}</a></h3><!-- /.blog-one__title -->
                                <p class="blog-one__text">{{$posts[2]->excerpt}}</p>
                                <!-- /.blog-one__text -->
                            </div><!-- /.blog-one__content -->
                        </div><!-- /.blog-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="blog-one__single">
                            <div class="blog-one__image">
                                <img src="{{$posts[1]->imageURL}}" alt="Post Image" />
                                <a a href="blog/{{$posts[1]->url}}"><i class="appyn-icon-plus-symbol"></i></a>
                            </div><!-- /.blog-one__image -->
                            <div class="blog-one__content">
                                <span class="blog-one__meta"><a href="#">{{$posts[1]->published_at->format('d m Y')}}</a></span>
                                <h3 class="blog-one__title"><a href="blog/{{$posts[1]->url}}">{{$posts[1]->title}}</a></h3><!-- /.blog-one__title -->
                                <p class="blog-one__text">{{$posts[1]->excerpt}}</p>
                                <!-- /.blog-one__text -->
                            </div><!-- /.blog-one__content -->
                        </div><!-- /.blog-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="blog-one__single">
                            <div class="blog-one__image">
                                <img src="{{$posts[0]->imageURL}}" alt="Post Image" />
                                <a a href="blog/{{$posts[0]->url}}"><i class="appyn-icon-plus-symbol"></i></a>
                            </div><!-- /.blog-one__image -->
                            <div class="blog-one__content">
                                <span class="blog-one__meta"><a href="#">{{$posts[0]->published_at->format('d m Y')}}</a></span>
                                <h3 class="blog-one__title"><a href="blog/{{$posts[0]->url}}">{{$posts[0]->title}}</a></h3><!-- /.blog-one__title -->
                                <p class="blog-one__text">{{$posts[0]->excerpt}}</p>
                                <!-- /.blog-one__text -->
                            </div><!-- /.blog-one__content -->
                        </div><!-- /.blog-one__single -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.blog-one -->
        <section class="subscribe-one">
            <div class="container">
                <div class="block-title text-center">
                    <h2 class="block-title__title">Suscríbete pra <br> recibir noticias</h2><!-- /.block-title__title -->
                </div><!-- /.block-title -->
                <form action="#" class="subscribe-one__form">
                    <input type="text" name="email" placeholder="Introduce tu email">
                    <button type="submit" class="subscribe-one__btn"><i class="fa fa-location-arrow"></i></button>
                </form><!-- /.subscribe-one__form -->
            </div><!-- /.container -->
        </section><!-- /.subscribe-one -->
        @extends('layouts.footer')
    </div><!-- /.page-wrapper -->
    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
    <!-- /.scroll-to-top -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/theme.js"></script>
</body>

</html>