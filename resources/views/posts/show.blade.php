<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$post->title}}</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/responsive.css">
</head>

<body>
    <div class="preloader"></div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one ">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="/">
                            <img src="/images/kondosbco.png" class="main-logo" width="97" alt="Logo" />
                        </a>
                        <button class="menu-toggler" data-target=".main-navigation">
                            <span class="fa fa-bars"></span>
                        </button>
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                        <ul class=" navigation-box one-page-scroll-menu ">
                         <li class="scrollToLink">
                                <a href="/#home">Inicio</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="/#services">Funciones</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="/#screens">Capturas</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="/#pricing">Costo</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="/#blog">Blog</a>
                                <ul class="sub-menu">
                                  <!--  <li><a href="blog.html">Ver todos los post</a></li> -->
                                </ul><!-- /.sub-menu -->
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class="right-side-box">
                        <a href="/login" class="thm-btn header-one__btn">Iniciar Sesión</a>
                    </div><!-- /.right-side-box -->
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.header-one -->
        <section class="inner-banner">
            <div class="container">
                <h2 class="inner-banner__title">{{$post->title}}</h2><!-- /.inner-banner__title -->
            </div><!-- /.container -->
        </section><!-- /.inner-banner -->
        <section class="blog-details">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-details__content">
                            <div class="blog-details__image">
                                <img src="{{$post->imageURL}}" alt="Post Image" />
                            </div><!-- /.blog-details__image -->
                            <div class="blog-details__top">
                                <span class="blog-one__meta"><a href="#">{{$post->published_at->format('M d Y')}}</a></span>
                            </div><!-- /.blog-details__top -->
                            <h3 class="blog-details__title">{{$post->title}}</h3><!-- /.blog-details__title -->
                            <p class="blog-details__text">{!!$post->body!!}</p><!-- /.blog-details__text -->
                        </div><!-- /.blog-details__content -->
                        <div class="share-block">
                            <div class="left-block">
                                <p>Tags:
                        @foreach($post->tags as $tag)
                        <a href="#">{{$tag->name}}</a>
                        @endforeach
                            </div><!-- /.left-block -->
                            <div class="social-block">
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                            </div><!-- /.social-block -->
                        </div><!-- /.share-block -->
                        <br>


                    </div><!-- /.col-lg-8 -->
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar__single sidebar__search">
                                <form action="#" class="sidebar__search-form">
                                    <input type="text" name="search" placeholder="Search here...">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div><!-- /.sidebar__single -->
                            <div class="sidebar__single sidebar__post">
                                <h3 class="sidebar__title">Latest Posts</h3><!-- /.sidebar__title -->
                                <div class="sidebar__post-wrap">

                                    @foreach($recentposts as $recentpost)
                                    <div class="sidebar__post__single">
                                        <div class="sidebar__post-image">
                                            <div class="inner-block"><img src="{{$recentpost->imageURL}}" alt="Post Image" /></div><!-- /.inner-block -->
                                        </div><!-- /.sidebar__post-image -->
                                        <div class="sidebar__post-content">
                                            <h4 class="sidebar__post-title"><a href="{{$recentpost->url}}"> {{$recentpost->title}}</a></h4><!-- /.sidebar__post-title -->
                                        </div><!-- /.sidebar__post-content -->
                                    </div><!-- /.sidebar__post__single -->
                                    @endforeach

                                </div><!-- /.sidebar__post-wrap -->
                            </div><!-- /.sidebar__single -->
                            <div class="sidebar__single sidebar__category">
                                <h3 class="sidebar__title">Categories</h3><!-- /.sidebar__title -->
                                <ul class="sidebar__category-list">

                        @forelse($totalcategories as $category)
                        <li class="sidebar__category-list-item"><a href="#">{{$category->name}}</a></li>
                        @empty
                        @endforelse
                                </ul><!-- /.sidebar__category-list -->
                            </div><!-- /.sidebar__single -->
                            <div class="sidebar__single sidebar__tags">
                                <h3 class="sidebar__title">Tags</h3><!-- /.sidebar__title -->
                                <ul class="sidebar__tags-list">

                        @forelse($totaltags as $tag)
                        <li class="sidebar__tags-list-item"><a href="#">{{$tag->name}}</a></li>
                        @empty
                        @endforelse
                                </ul><!-- /.sidebar__category-list -->
                            </div><!-- /.sidebar__single -->
                        </div><!-- /.sidebar -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.blog-details -->
 @extends('layouts.footer')
    </div><!-- /.page-wrapper -->
    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
    <!-- /.scroll-to-top -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/waypoints.min.js"></script>
    <script src="/js/jquery.counterup.min.js"></script>
    <script src="/js/waypoints.min.js"></script>
    <script src="/js/jquery.counterup.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/swiper.min.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/theme.js"></script>
</body>

</html>
