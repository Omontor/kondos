@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Crear Post</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.posts.index')}}">Posts</a></li>
              <li class="breadcrumb-item active">Crear</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
<form method="POST" action="{{route('admin.posts.store')}}">
  {{csrf_field()}}
 <div class="row mb-2">
  
<div class="col-md-8">
        <div class="card card-primary">
            <div class="card-header">
            </div>         
                     <div class="card-body">
                        <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">
                      <label>Título de la Publicación</label>
                <input name="title" 
                class="form-control" 
                placeholder="Ingresa el Título de la Publicación" 
                value="{{old('title')}}">
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                      </div>
                <div class="form-group {{$errors->has('body') ? 'is-invalid' : ''}}">
                <label>Contenido de la Publicación</label>
                        <textarea id="editor1" name="body" rows="10" cols="80" placeholder="Introduce el contenido de tu post">
                         {{old('body')}}
                    </textarea>
                    {!!$errors->first('body', ' <span class="help-block" style= "color:red;">:message</span>')!!}
              </div>
               
              </div>   
          </div>

        </div>

            <div class="col-md-4">
        <div class="card card-primary">
            <div class="card-header">
            </div>
            <div class="card-body">
                  <div class="form-group">
                  <label>Date range:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input name="published_at" 
                    type="text" 
                    class="form-control float-right" 
                    id="datepicker" 
                    value="{{old('published_at')}}">
                  </div>
                  <!-- /.input group -->
                </div> 

                <div class="form-group {{$errors->has('category') ? 'is-invalid' : ''}}">
                  
                  <label>Categorías</label>
                  <select name="category" class="form-control">
                    <option value=""> 
                      Selecciona Una Categoría
                    </option>


                    @forelse($categories as $category)
                    <option value="{{$category->id}}"

                      {{old('category') == $category->id ? 'selected' : '' }}
                      >{{$category->name}}</option>
                    @empty
                    @endforelse
                  </select>
{!!$errors->first('category', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>

                <div class="form-group {{$errors->has('tags') ? 'is-invalid' : ''}}">
                  <label>Etiquetas</label>
                    <select name="tags[]" class="select2" multiple="multiple" data-placeholder="Selecciona Una o Más Etiquetas" style="width: 100%;">

                      @forelse($tags as $tag)
                      <option {{collect(old('tags'))->contains($tag->id) ? 'selected' : ''}} value="{{$tag->id}}">{{$tag->name}}</option>
                      @empty
                      @endforelse
                  </select>
                  {!!$errors->first('tags', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                </div>
                
 
                  <!-- /.input group -->
               
                <div class="form-group {{$errors->has('excerpt') ? 'is-invalid' : ''}}">
                <label>Resumen Publicación</label>
                <textarea name="excerpt" class="form-control" placeholder="Ingresa el Clickbait de la Publicación"></textarea>
                {!!$errors->first('excerpt', ' <span class="help-block" style= "color:red;">:message</span>')!!}
              </div>

              <div class="form-group">
                
                <button type="submit" class="btn btn-primary btn-block">
                  Guardar Publicación
                </button>
              </div>

  </form>
            </div>
          </div>
    </div>

</div>
@endsection


@push('styles')

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<!-- bootstrap datepicker -->
<script src="/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
<script>

    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
    })
  });
</script>
@endpush

