<!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

            <li class="nav-item">

<li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-pencil-alt"></i>
              <p>
                Blog
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.posts.index')}}" class="nav-link">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos los Post</p>
                </a>
              </li>
              <li class="nav-item">
                  <a href="{{route('admin.posts.create')}}" class="nav-link">
                  <i class="fas fa-pencil-alt nav-icon"></i>
                  <p>Crear Post</p>
                </a>
              </li>
            </ul>
          </li>

              <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-alt"></i>
              <p>
                Administradores
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.administrators.index')}}" class="nav-link">
                <i class="far fa-eye nav-icon"></i>
                  <p>Todos</p>
                </a>
              </li>
            </ul>
          </li>
          </li>

              <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Anunciantes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.announcers.index')}}" class="nav-link">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Ver Todos</p>
                </a>
              </li>
            </ul>
          </li>
          </li>
              <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Condominios
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.condos.index')}}" class="nav-link">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Todos</p>
                </a>
              </li>
            </ul>
          </li>
          </li>

              <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Viviendas
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.homes.index')}}" class="nav-link">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Todas</p>
                </a>
              </li>
            </ul>
          </li>
          </li>

              <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-ad"></i>
              <p>
                Publicidad
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a href="{{route('admin.ads.index')}}" class="nav-link">
                  <i class="far fa-eye nav-icon"></i>
                  <p>Todos</p>
                </a>
              </li>
            </ul>
          </li>
          </li>

          </li>

          </li>

        </ul>
      </nav>
