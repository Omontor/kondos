@extends('admin.layout')
@section('header')
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Posts</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Posts</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
@stop
@section('content')
          <div class="card">
            <div class="card-header" style="vertical-align:  middle;">
              <h3 class="card-title" style="vertical-align:  middle;">Listado de Publicaciones</h3>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModalLong">
+ Crear publicación
</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Título</th>
                  <th>Extracto</th>
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                	@forelse($posts as $post)
               <tr>
                 <td>{{$post->id}}</td>
                  <td>{{$post->title}}</td>
                  <td>{{$post->excerpt}}</td>
                  <td>

                  	<a class="btn btn-info btn-sm" href=""><i class="fas fa-pencil-alt"></i></a>
                  	<a class="btn btn-danger btn-sm" href=""><i class="fas fa-times"></i></a>

                  </td>

                   </tr>
                	@empty
                	<tr>
                		<td>No hay datos para mostrar</td>
                	</tr>
                	@endforelse
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
@endsection



@push('styles')
    <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <form method="POST" action="{{route('admin.posts.store')}}">
  {{csrf_field()}}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Agrega el título de tu nueva publicación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                               <div class="form-group {{$errors->has('title') ? 'is-invalid' : ''}}">

                <input name="title"
                class="form-control"
                placeholder="Ingresa el Título de la Publicación"
                value="{{old('title')}}">
                </input>
               {!!$errors->first('title', ' <span class="help-block" style= "color:red;">:message</span>')!!}
                      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button  class="btn btn-primary">Crear Publicación</button>
      </div>
    </div>
  </div>
</form>
</div>


@endpush