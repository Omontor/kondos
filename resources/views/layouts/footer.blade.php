<footer class="site-footer">
            <div class="site-footer__upper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="footer-widget">
                                <a href="/" class="footer-widget__logo"><img src="/images/kondosbco.png"
                                        width="132" alt="Awesome Image" /></a>
                                <p class="footer-widget__text">Kondos.<br>Sistema de administración de condominios</p><!-- /.footer-widget__text -->
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-5 -->
                        <div class="col-lg-4">
                            <div class="footer-widget">
                                <ul class="footer-widget__links">
                                    <li class="footer-widget__links-item"><a href="#">Inicio</a></li>
                                    <li class="footer-widget__links-item"><a href="#">Funciones</a></li>
                                    <li class="footer-widget__links-item"><a href="#">Capturas</a></li>
                                    <li class="footer-widget__links-item"><a href="#">Costo</a></li>
                                    <li class="footer-widget__links-item"><a href="#">Blog</a></li>
                                </ul><!-- /.footer-widget__links -->
                                <ul class="footer-widget__links">
                                    <li class="footer-widget__links-item"><a href="#">Contacto</a></li>
                                    <li class="footer-widget__links-item"><a href="#">Privacidad</a></li>
                                    <li class="footer-widget__links-item"><a href="#">Términos y Condiciones</a></li>
    
                                </ul><!-- /.footer-widget__links -->
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-3">
                            <div class="footer-widget">
                                <a href="#" class="footer-widget__btn">
                                    <i class="fa fa-play"></i>
                                    <span class="footer-widget__btn-text">
                                        Descargar en <span class="footer-widget__btn-text-highlight">Google Play</span>
                                    </span>
                                </a>
                                <a href="#" class="footer-widget__btn">
                                    <i class="fa fa-play"></i>
                                    <span class="footer-widget__btn-text">
                                        Descargar en <span class="footer-widget__btn-text-highlight">play store</span>
                                    </span>
                                </a>
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-3 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.site-footer__upper -->
            <div class="site-footer__bottom">
                <div class="container">
                    <div class="site-footer__social">
                        <a href="#"><i class="fa fa-facebook-square"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                    </div><!-- /.site-footer__social -->
                    <p class="site-footer__copy-text"><i class="fa fa-copyright"></i>{{ now()->year }} <a
                            href="#">{{config('app.name')}}</a> by Peanut Agency</p><!-- /.site-footer__copy-text -->
                </div><!-- /.container -->
            </div><!-- /.site-footer__bottom -->
        </footer><!-- /.site-footer -->
