<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home () {



            return view('welcome', [

            'posts' => Post::latest('id', 'DESC')->paginate(3),
             'totaltags' => Tag::latest('id', 'DESC')->paginate(3)
        ]);


    }
}
