<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Tag;
use App\Category;
class PostsController extends Controller
{

    public function show(Post $post) {

    	$totaltags = Tag::all();
    	$totalcategories = Category::all();
    	$recentposts = Post::latest('id', 'DESC')->paginate(3);
    	return view('posts.show', compact('post', 'totaltags', 'totalcategories', 'recentposts'));

    }
}
