<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdministratorsController extends Controller
{
        public function indexadmins () {

    	$users = User::all();
    	return view('admin.administrators.index', compact('users'));
    }

    public function create () {


    }

    public function store (Request $request) {


    }
}
