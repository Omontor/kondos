<?php

namespace App\Http\Controllers\Admin;
use App\Condo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CondosController extends Controller
{
       public function indexcondos () {

    	$condos = Condo::all();
    	return view('admin.condos.index', compact('condos'));
    }
}
