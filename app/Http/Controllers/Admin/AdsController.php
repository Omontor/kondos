<?php

namespace App\Http\Controllers\Admin;
use App\Ad;
use App\Announcer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdsController extends Controller
{
         public function indexads () {

    	$ads = Ad::all();
    	$announcers = Announcer::all();
    	return view('admin.ads.index', compact('ads', 'announcers'));
    }
}
