<?php

namespace App\Http\Controllers\Admin;
use App\Home;
use App\Condo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomesController extends Controller
{

       public function indexhomes () {

    	$homes = Home::all();
    	$condos = Condo::all();
    	return view('admin.homes.index', compact('homes', 'condos'));
    }
}
