<?php

namespace App\Http\Controllers\Admin;
use App\Announcer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnnouncersController extends Controller
{
     public function indexannouncers () {

    	$announcers = Announcer::all();
    	return view('admin.announcers.index', compact('announcers'));
    }

    public function create () {


    }
}
