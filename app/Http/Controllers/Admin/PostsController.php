<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Tag;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PostsController extends Controller
{
    public function index () {

    	$posts = Post::all();
    	return view('admin.posts.index', compact('posts'));
    }

    public function create () {

    	$categories = Category::all();
    	$tags = Tag::all();
    	return view('admin.posts.create', compact('categories', 'tags'));
    }

    public function store (Request $request) {

        $this->validate($request, [

            'title' => 'required',
            'body' => 'required',
            'category' => 'required',
            'excerpt' => 'required',
            'tags' => 'required'
        ]);

    	$post = new Post;
    	$post->title = $request->get('title');
        $post->url = str_slug($request->get('title'));
    	$post->excerpt = $request->get('excerpt');
    	$post->body = $request-> get('body');
    	$post->published_at = $request->has('published_at') ? Carbon::parse($request->get('published_at')) : null;
    	$post->category_id = $request->get('category');
    	
    	$post->save();
		$post->tags()->attach($request->get('tags'));
    	 return back()->with('flash', 'Tu publicación ha sido creada');

    }
}
