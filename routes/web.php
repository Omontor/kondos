<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('blog/{post}', 'PostsController@show');


//Métodos con el prefijo Admin

Route::group(['prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => 'auth'],
    function() {
        //rutas de administración
    Route::get('/','AdminController@index')->name('dashboard');
    Route::get('posts', 'PostsController@index')->name('admin.posts.index');
    Route::get('posts/create', 'PostsController@create')->name('admin.posts.create');
    Route::post('posts', 'PostsController@store')->name('admin.posts.store');


    Route::get('administrators', 'AdministratorsController@indexadmins')->name('admin.administrators.index');
    Route::get('administrators/create', 'AdministratorsController@create')->name('admin.administrators.create');
    Route::post('administrators', 'AdministratorsController@store')->name('admin.administrators.store');

    Route::get('announcers', 'AnnouncersController@indexannouncers')->name('admin.announcers.index');
    Route::get('announcers/create', 'AnnouncersController@create')->name('admin.announcers.create');
    Route::post('announcers', 'AnnouncersController@store')->name('admin.announcers.store');

    Route::get('condos', 'CondosController@indexcondos')->name('admin.condos.index');
    Route::get('condos/create', 'CondosController@create')->name('admin.condos.create');
    Route::post('condos', 'CondosController@store')->name('admin.condos.store');

    Route::get('homes', 'HomesController@indexhomes')->name('admin.homes.index');
    Route::get('homes/create', 'HomesController@create')->name('admin.homes.create');
    Route::post('homes', 'HomesController@store')->name('admin.homes.store');

    Route::get('ads', 'AdsController@indexads')->name('admin.ads.index');
    Route::get('ads/create', 'AdsController@create')->name('admin.ads.create');
    Route::post('ads', 'AdsController@store')->name('admin.ads.store');

  //  Route::get('banners', 'BannersController@index')->name('admin.banners.index');
  //  Route::get('banners/create', 'BannersController@create')->name('admin.banners.create');
  //  Route::post('banners', 'BannersController@store')->name('admin.banners.store');


});




//Route::get('/home', 'HomeController@index')->name('home');

//Quitar para desactivar registro
Auth::routes();
        // Authentication Routes...
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'Auth\RegisterController@register');

        Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');